package ru.medianik.lab3;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        byte[] bytes = Files.readAllBytes(Path.of("kotik.bmp"));
        Encryptor encryptor = new Encryptor(bytes);
        byte[] bytes1 = encryptor.encrypt("Hello world".getBytes(StandardCharsets.UTF_8));
        Files.write(Path.of("kotik1.bmp"), bytes1);
        var decrypted = encryptor.decrypt();
        System.out.println(new String(decrypted));
    }

    static class Encryptor {
        private final byte[] data;
        private int position;

        Encryptor(byte[] data) {
            this.data = data;
        }

        public byte[] encrypt(byte[] bytes) {
            position = 54;
            if (data.length < bytes.length * 4)
                throw new IllegalStateException();
            for (byte b : bytes) {
                encryptSingleByte(b);
            }
            encryptSingleByte((byte) -1);
            encryptSingleByte((byte) -1);
            return data;
        }

        private void encryptSingleByte(byte b) {
            data[position] &= (0xFC);
            data[position] |= (b >> 6) & 0x3;
            data[position + 1] &= (0xFC);
            data[position + 1] |= (b >> 4) & 0x3;
            data[position + 2] &= (0xFC);
            data[position + 2] |= (b >> 2) & 0x3;
            data[position + 3] &= (0xFC);
            data[position + 3] |= (b) & 0x3;
            position += 4;
        }

        public byte[] decrypt() {
            position = 54;
            List<Byte> bytes = new ArrayList<>();
            byte current;
            byte next;
            byte next1;
            current = readSingleByte();
            next = readSingleByte();
            next1 = readSingleByte();
            bytes.add(current);

            while (next1 != -1 || next != -1) {
                current = next;
                next = next1;
                next1 = readSingleByte();
                bytes.add(current);
            }
            return convertIntegers(bytes);
        }

        public static byte[] convertIntegers(List<Byte> integers) {
            byte[] ret = new byte[integers.size()];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = integers.get(i);
            }
            return ret;
        }

        private byte readSingleByte() {
            byte b = 0;
            b |= (data[position] & 0x3) << 6;
            b |= (data[position + 1] & 0x3) << 4;
            b |= (data[position + 2] & 0x3) << 2;
            b |= (data[position + 3] & 0x3);
            position += 4;
            return b;
        }
    }
}